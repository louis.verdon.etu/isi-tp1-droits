#Dosier a
touch dir_a/a.txt
echo blabla > dir_a/a.txt
cat dir_a/a.txt
mv dir_a/a.txt dir_a/a_renomme.txt
mv dir_a/a_renomme.txt dir_a/a.txt #Pour que le nom du fichier reste le même
rm dir_a/a.txt

echo blabla > dir_a/text.txt
cat dir_a/text.txt
mv dir_a/text.txt dir_a/text_renomme.txt
mv dir_a/text_renomme.txt dir_a/text.txt #Pour que le nom du fichier reste le même
rm dir_a/text.txt

#Dosier b
touch dir_b/a.txt
echo blabla > dir_b/a.txt
cat dir_b/a.txt
mv dir_b/a.txt dir_b/a_renomme.txt
mv dir_b/a_renomme.txt dir_b/a.txt #Pour que le nom du fichier reste le même
rm dir_b/a.txt

echo blabla > dir_b/text.txt
cat dir_b/text.txt
mv dir_b/text.txt dir_b/text_renomme.txt
mv dir_b/text_renomme.txt dir_b/text.txt #Pour que le nom du fichier reste le même
rm dir_b/text.txt

#Dosier c
touch dir_c/a.txt
echo blabla > dir_c/a.txt
cat dir_c/a.txt
mv dir_c/a.txt dir_c/a_renomme.txt
mv dir_c/a_renomme.txt dir_c/a.txt #Pour que le nom du fichier reste le même
rm dir_c/a.txt

echo blabla > dir_c/text.txt
cat dir_c/text.txt
mv dir_c/text.txt dir_c/text_renomme.txt
mv dir_c/text_renomme.txt dir_c/text.txt #Pour que le nom du fichier reste le même
rm dir_c/text.txt
