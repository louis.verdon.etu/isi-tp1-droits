#Dosier a
touch dir_a/b.txt
echo blabla > dir_a/b.txt
cat dir_a/b.txt
mv dir_a/b.txt dir_a/b_renomme.txt
mv dir_a/b_renomme.txt dir_a/b.txt #Pour que le nom du fichier reste le même
rm dir_a/b.txt

echo blabla > dir_a/text.txt
cat dir_a/text.txt
mv dir_a/text.txt dir_a/text_renomme.txt
mv dir_a/text_renomme.txt dir_a/text.txt #Pour que le nom du fichier reste le même
rm dir_a/text.txt

#Dosier b
touch dir_b/b.txt
echo blabla > dir_b/b.txt
cat dir_b/b.txt
mv dir_b/b.txt dir_b/b_renomme.txt
mv dir_b/b_renomme.txt dir_b/b.txt #Pour que le nom du fichier reste le même
rm dir_b/b.txt

echo blabla > dir_b/text.txt
cat dir_b/text.txt
mv dir_b/text.txt dir_b/text_renomme.txt
mv dir_b/text_renomme.txt dir_b/text.txt #Pour que le nom du fichier reste le même
rm dir_b/text.txt

#Dosier c
touch dir_c/b.txt
echo blabla > dir_c/b.txt
cat dir_c/b.txt
mv dir_c/b.txt dir_c/b_renomme.txt
mv dir_c/b_renomme.txt dir_c/b.txt #Pour que le nom du fichier reste le même
rm dir_c/b.txt

echo blabla > dir_c/text.txt
cat dir_c/text.txt
mv dir_c/text.txt dir_c/text_renomme.txt
mv dir_c/text_renomme.txt dir_c/text.txt #Pour que le nom du fichier reste le même
rm dir_c/text.txt
