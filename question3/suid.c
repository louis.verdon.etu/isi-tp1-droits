#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

#define EXIT_FAILURE 1
#define EXIT_SUCCESS 0
#define STRING_SIZE 2000

int main(int argc, char *argv[])
{
  FILE *f;
  char line[STRING_SIZE];
  if (argc < 2) {
    printf("Missing argument\n");
    exit(EXIT_FAILURE);
  }
  f = fopen(argv[1], "r");
  if (f == NULL) {
    perror("Cannot open file");

    exit(EXIT_FAILURE);
  }
  printf("File opens correctly\n");
  printf("\n");
  uid_t real_uid = getuid();
  uid_t effect_uid = geteuid();
  gid_t real_gid = getgid();
  gid_t effect_gid = getegid();
  printf("ruid=%d euid=%d\n", real_uid, effect_uid);
  printf("rgid=%d egid=%d\n", real_gid, effect_gid);

  while (fgets(line, STRING_SIZE, f) != NULL) {
    printf("%s", line);
  }

  fclose(f);
  exit(EXIT_SUCCESS);
  // TODO
  return 0;
}
