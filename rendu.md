# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Nom, Prénom, email: Tubeuf David david.tubeuf.etu@univ-lille.fr

- Nom, Prénom, email: Verdon Louis louis.verdon.etu@univ-lille.fr

Pour lancer la machine virtuelle, on lance la commande :
```sh
ssh -i ~/.ssh/ma-paire.pem ubuntu@172.28.101.7
```

## Question 1

``` sh
sudo adduser toto
sudo adduser toto ubuntu
```
toto appartient au groupe ubuntu(1000) mais a pour groupe principal toto(1001).  
Le processus ne peut pas ouvrir en écriture le fichier `myfile.txt` car en tant que propriétaire il ne possède pas des droits d'écriture.

## Question 2

Le caractère x permet à l'utilisateur de pouvoir accéder au dossier.

Notre dossier `testDossier` correspond au dossier `mydir` de l'énoncé.  

```sh
mkdir testDossier
chmod 765 testDossier
touch testDossier/data.txt
su toto
cd testDossier

cd: testDossier/: Permission denied

ls -al testDossier

ls: cannot access 'testDossier/.': Permission denied
ls: cannot access 'testDossier/..': Permission denied
ls: cannot access 'testDossier/data.txt': Permission denied
total 0
d????????? ? ? ? ?            ? .
d????????? ? ? ? ?            ? ..
-????????? ? ? ? ?            ? data.txt
```

toto ne peut pas ouvrir le dossier `testDossier` car, appartenant au groupe ubuntu, il ne possède pas les droits d'exécution.  
Il peut tout de même connaître le contenu du répertoire, mais n'a accès à aucune information sur les dossiers/fichiers contenus.  

## Question 3

On copie le programme c dans la machine virtuelle :
```sh
scp -i ~/.ssh/ma-paire.pem question3/suid.c ubuntu@172.28.101.7:~
```

On teste le programme c avec le processus ubuntu :
``` sh
gcc suid.c

echo "pomme de terre" > testDossier/data.txt
./a.out testDossier/data.txt

File opens correctly

ruid=1000 euid=1000
rgid=1000 egid=1000
pomme de terre
```

On teste le programme avec le processus toto :
``` sh
su toto
./a.out testDossier/data.txt

Cannot open file: Permission denied
```

Les ids sont tous égaux à 1000 (qui correspond à ubuntu).  
Et non, le processus n'arrive pas à lire le fichier en lecture (Permission denied)

```sh
exit
chmod u+s a.out
su toto
./a.out testDossier/data.txt
File opens correctly

ruid=1001 euid=1000
rgid=1001 egid=1001
pomme de terre
```

ruid, rgid et egid sont les ids de toto.  
euid est cependant l'id d'ubuntu, donc le propriétaire du fichier exécutable.  
toto arrive à lire le fichier.

## Question 4

On copie le script python dans la machine virtuelle :
```sh
scp -i ~/.ssh/ma-paire.pem question4/suid.py ubuntu@172.28.101.7:~
```

On active le set-user-id et on lance le script avec toto :
``` sh
chmod u+s suid.py
su toto
python3 suid.py

User id 1001
Group id 1001
```

Ici par contre, les deux ids sont à 1001, donc ceux de toto.

## Question 5

La commande `chfn` permet de changer les informations de l'utilisateur, comme son nom, son numéro de salle, etc ...  

```sh
ls -al /usr/bin/chfn

-rwsr-xr-x 1 root root 85064 May 28  2020 /usr/bin/chfn
```

On voit que le set-user-id a été utilisé pour le programme chfn, cela permet par exemple à un utilisateur de changer ses propres informations sans être l'administrateur.

Après avoir modifié les informations de toto grâce à `chfn` :
```sh
chfn toto
Password: 
Changing the user information for toto
Enter the new value, or press ENTER for the default
	Full Name: 
	Room Number []: 123
	Work Phone []: 1234
	Home Phone []: 123456
```

Les informations de toto ont bien été modifiées en :
```sh
toto:x:1001:1001:,123,1234,123456:/home/toto:/bin/bash
```


## Question 6

Si un utilisateur possède un x qui suit son nom dans /etc/passwd, alors son mot de passe est crypté et stocké dans etc/shadow.  

On ne veut pas stocker les mots de passe "en dur" donc on les crypte, et l'accès aux mots de passe ne nécessite pas les mêmes droits que les informations classiques donc ils doivent être stockés dans un autre fichier à part.  

On trouve donc cette partie concernant l'utilisateur toto dans le fichier etc/shadow :
```sh
toto:$6$2yE2rrp3CsonQh.c$fuMQMWy1U42BpuC/Y.2aicEqORk5Yr92GKv1v.q7MkS76.uF0eLaxtBYSQNt76cSTI69zIluveStOneQki6Wi1:19004:0:99999:7:::
```


## Question 7

On crée les utilisateurs et on les associe à leurs groupes respectifs :
```sh
sudo adduser lambda_a
sudo addgroup groupe_a
sudo adduser lambda_a groupe_a

sudo adduser lambda_b
sudo addgroup groupe_b
sudo adduser lambda_b groupe_b

sudo adduser --system admin
```

L'admin a tous les droits sur les répertoires donc on les crée en le nommant propriétaire :  
```sh
su admin

mkdir dir_a
mkdir dir_b
mkdir dir_c

exit
```

Les répertoires dir_a et dir_b ont des comportements différents selon le groupe de l'utilisateur qui y accède, donc on leur donne respectivement les groupes groupe_a et groupe_b :
```sh
sudo chgrp groupe_a dir_a
sudo chgrp groupe_b dir_b
```

On change les droits d'accès aux dossiers : 
 - leur propriétaire a tous les droits
 - les utilisateurs du même groupe que les dossiers peuvent lire, écrire et exécuter pour dir_a et dir_b, mais les autres utilisateurs n'ont aucun droit
 - les utilisateurs des deux groupes peuvent lire et exécuter dans dir_c, mais pas écrire
```sh
sudo chmod 770 dir_a
sudo chmod 770 dir_b
sudo chmod 705 dir_c
```

Seul le propriétaire (admin) des 3 dossiers peut supprimer ou renommer les fichiers créés par n'importe qui, on ajoute donc un sticky-bit à ces 3 dossiers :
```sh
su admin

sudo chmod +t dir_a
sudo chmod +t dir_b
sudo chmod +t dir_c
```

Pour tester les droits des 3 utilisateurs sur les 3 dossiers, on crée en amont un fichier par dossier :
```sh
touch dir_a/text.txt
touch dir_b/text.txt
touch dir_b/text.txt

echo "test" >  dir_a/text.txt
echo "test" >  dir_b/text.txt
echo "test" >  dir_b/text.txt
exit
```

On teste le script `lambda_a.sh` avec l'utilisateur lambda_a :
```sh
./lambda_a.sh

blabla
blabla
touch: cannot touch 'dir_b/a.txt': Permission denied
./lambda_a.sh: line 17: dir_b/a.txt: Permission denied
cat: dir_b/a.txt: Permission denied
mv: failed to access 'dir_b/a_renomme.txt': Permission denied
mv: failed to access 'dir_b/a.txt': Permission denied
rm: cannot remove 'dir_b/a.txt': Permission denied
./lambda_a.sh: line 23: dir_b/text.txt: Permission denied
cat: dir_b/text.txt: Permission denied
mv: failed to access 'dir_b/text_renomme.txt': Permission denied
mv: failed to access 'dir_b/text.txt': Permission denied
rm: cannot remove 'dir_b/text.txt': Permission denied
touch: cannot touch 'dir_c/a.txt': Permission denied
./lambda_a.sh: line 31: dir_c/a.txt: Permission denied
cat: dir_c/a.txt: No such file or directory
mv: cannot stat 'dir_c/a.txt': No such file or directory
mv: cannot stat 'dir_c/a_renomme.txt': No such file or directory
rm: cannot remove 'dir_c/a.txt': No such file or directory
./lambda_a.sh: line 37: dir_c/text.txt: Permission denied
cat: dir_c/text.txt: No such file or directory
mv: cannot stat 'dir_c/text.txt': No such file or directory
mv: cannot stat 'dir_c/text_renomme.txt': No such file or directory
rm: cannot remove 'dir_c/text.txt': No such file or directory
```

On teste le script `lambda_b.sh` avec l'utilisateur lambda_b :
```sh
./lambda_b.sh

touch: cannot touch 'dir_a/b.txt': Permission denied
./lambda_b.sh: line 3: dir_a/b.txt: Permission denied
cat: dir_a/b.txt: Permission denied
mv: failed to access 'dir_a/b_renomme.txt': Permission denied
mv: failed to access 'dir_a/b.txt': Permission denied
rm: cannot remove 'dir_a/b.txt': Permission denied
./lambda_b.sh: line 9: dir_a/text.txt: Permission denied
cat: dir_a/text.txt: Permission denied
mv: failed to access 'dir_a/text_renomme.txt': Permission denied
mv: failed to access 'dir_a/text.txt': Permission denied
rm: cannot remove 'dir_a/text.txt': Permission denied
blabla
blabla
touch: cannot touch 'dir_c/b.txt': Permission denied
./lambda_b.sh: line 31: dir_c/b.txt: Permission denied
cat: dir_c/b.txt: No such file or directory
mv: cannot stat 'dir_c/b.txt': No such file or directory
mv: cannot stat 'dir_c/b_renomme.txt': No such file or directory
rm: cannot remove 'dir_c/b.txt': No such file or directory
./lambda_b.sh: line 37: dir_c/text.txt: Permission denied
cat: dir_c/text.txt: No such file or directory
mv: cannot stat 'dir_c/text.txt': No such file or directory
mv: cannot stat 'dir_c/text_renomme.txt': No such file or directory
rm: cannot remove 'dir_c/text.txt': No such file or directory
```

On teste le script `admin.sh` avec l'utilisateur admin :
```sh
./admin.sh

blabla
blabla
blabla
blabla
blabla
blabla
```


## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests.
